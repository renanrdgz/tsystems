﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TorneioLuta.Controllers;
using TorneioLuta.Models;
using System.Collections.Generic;
using System.Linq;

namespace TorneioLuta.Tests
{
    [TestClass]
    public class LutadorControllerTest
    {
        [TestMethod]
        public void TesteOrdernarVencedor()
        {
            List<LutadorMOD> listaLutadores = new List<LutadorMOD>();
            listaLutadores.Add(new LutadorMOD() { Id = 1, Nome = "lutador1", Idade = 25, ArtesMarciais = new string[] { "Boxe"}, Lutas = 10, Derrotas = 3, Vitorias = 10 });
            listaLutadores.Add(new LutadorMOD() { Id = 2, Nome = "lutador2", Idade = 32, ArtesMarciais = new string[] { "Boxe" }, Lutas = 5, Derrotas = 2, Vitorias = 3 });
            listaLutadores.Add(new LutadorMOD() { Id = 3, Nome = "lutador3", Idade = 40, ArtesMarciais = new string[] { "Boxe" }, Lutas = 20, Derrotas = 0, Vitorias = 20 });

            var controller = new LutadorController();
            List<LutadorMOD> listaOrdenada = controller.OrdenarVencedor(listaLutadores);

            bool resultado = false;

            if (listaOrdenada[0].Id == 3 && listaOrdenada[1].Id == 1 && listaOrdenada[2].Id == 2)
                resultado = true;

            Assert.AreEqual(resultado, true);
        }
    }
}
