﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TorneioLuta.Models
{
    public class LutadorMOD
    {
        [Display(Name = "Código")]
        public int Id { get; set; }

        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Display(Name = "Idade")]
        public int Idade { get; set; }

        [Display(Name = "Artes Marciais")]
        public string[] ArtesMarciais { get; set; }

        [Display(Name = "Lutas")]
        public double Lutas { get; set; }

        [Display(Name = "Derrotas")]
        public int Derrotas { get; set; }

        [Display(Name = "Vitórias")]
        public double Vitorias { get; set; }
    }
}