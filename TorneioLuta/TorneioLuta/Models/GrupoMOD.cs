﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TorneioLuta.Models
{
    public class GrupoMOD
    {
        [Display(Name = "Código")]
        public string Codigo { get; set; }

        [Display(Name = "Lutadores")]
        public List<LutadorMOD> Lutadores { get; set; }
    }
}