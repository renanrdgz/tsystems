﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TorneioLuta.Data;
using TorneioLuta.Models;

namespace TorneioLuta.Controllers
{
    public class LutadorController : Controller
    {

        public ActionResult Index()
        {
            try
            {
                List<LutadorMOD> listaLutadores = DadosAPI.ObterLutadoresAPI();

                return View(listaLutadores);
            }
            catch (Exception erro)
            {
                TempData["Erro"] = String.Concat("Detalhes: " + erro.Message);

                return View();
            }
        }

        public ActionResult Resultado()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Selecionar(FormCollection dadosForm)
        {
            try
            {
                if (this.VerificarQuantidadeLutadores(dadosForm))
                {
                    List<int> lutadoresSelecionados = dadosForm["checkboxLutador"].Split(',').Select(o => int.Parse(o)).ToList();

                    List<LutadorMOD> listaLutadores = DadosAPI.ObterLutadoresAPI();

                    List<GrupoMOD> listaGrupos = this.CriarGrupos(listaLutadores, lutadoresSelecionados);

                    List<GrupoMOD> listaVencedoresGrupos = this.ObterLutadoresFaseGrupos(listaGrupos);

                    List<GrupoMOD> listaVencedoresQuartas = this.ObterVencedoresQuartasFinal(listaVencedoresGrupos);

                    List<GrupoMOD> listaVencedoresSemi = this.ObterVencedoresSemifinal(listaVencedoresQuartas);

                    List<LutadorMOD> listaVencedoresTorneio = this.ObterVencedoresTorneio(listaVencedoresSemi);

                    return View("Resultado", listaVencedoresTorneio);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception erro)
            {
                TempData["Erro"] = String.Concat("Detalhes: " + erro.Message);

                return View();
            }
        }

        #region MÉTODOS

        private List<GrupoMOD> CriarGrupos(List<LutadorMOD> listaLutadores, List<int> lutadoresSelecionados)
        {
            try
            {
                List<GrupoMOD> listaGrupos = new List<GrupoMOD>();

                //Obtém os lutadores por id e ordena por idade
                List<LutadorMOD> listaSelecionados = listaLutadores.Where(o => lutadoresSelecionados.Contains(o.Id)).OrderBy(o => o.Idade).ToList();

                //Grupo A
                listaGrupos.Add(new GrupoMOD() { Codigo = "A", Lutadores = listaSelecionados.Take(5).ToList() });

                //Grupo B
                listaGrupos.Add(new GrupoMOD() { Codigo = "B", Lutadores = listaSelecionados.Skip(5).Take(5).ToList() });

                //Grupo C
                listaGrupos.Add(new GrupoMOD() { Codigo = "C", Lutadores = listaSelecionados.Skip(10).Take(5).ToList() });

                //Grupo D
                listaGrupos.Add(new GrupoMOD() { Codigo = "D", Lutadores = listaSelecionados.Skip(15).Take(5).ToList() });

                return listaGrupos;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<GrupoMOD> ObterLutadoresFaseGrupos(List<GrupoMOD> listaGrupos)
        {
            try
            {
                List<GrupoMOD> listaVencedoresDuplasGrupos = new List<GrupoMOD>();

                foreach (GrupoMOD grupo in listaGrupos)
                {
                    listaVencedoresDuplasGrupos.Add(new GrupoMOD()
                    {
                        Codigo = grupo.Codigo,
                        Lutadores = this.OrdenarVencedor(grupo.Lutadores).Take(2).ToList()
                    });
                }

                return listaVencedoresDuplasGrupos;
            }
            catch (Exception erro)
            {

                throw;
            }
        }

        public List<GrupoMOD> ObterVencedoresQuartasFinal(List<GrupoMOD> listaVencedoresGrupos)
        {
            try
            {
                List<GrupoMOD> listaVencedoresQuartas = new List<GrupoMOD>();

                //Grupo AB
                GrupoMOD grupoAB = new GrupoMOD();
                grupoAB.Codigo = "AB";
                grupoAB.Lutadores = new List<LutadorMOD>();
                grupoAB.Lutadores.Add(this.OrdenarVencedor(new List<LutadorMOD>() { listaVencedoresGrupos.SingleOrDefault(o => o.Codigo == "A").Lutadores.First(),
                                                                                    listaVencedoresGrupos.SingleOrDefault(o => o.Codigo == "B").Lutadores.Last() })
                                                                                    .First());

                grupoAB.Lutadores.Add(this.OrdenarVencedor(new List<LutadorMOD>() { listaVencedoresGrupos.SingleOrDefault(o => o.Codigo == "A").Lutadores.Last(),
                                                                                    listaVencedoresGrupos.SingleOrDefault(o => o.Codigo == "B").Lutadores.First() })
                                                                                    .First());

                //Grupo CD
                GrupoMOD grupoCD = new GrupoMOD();
                grupoCD.Codigo = "CD";
                grupoCD.Lutadores = new List<LutadorMOD>();
                grupoCD.Lutadores.Add(this.OrdenarVencedor(new List<LutadorMOD>() { listaVencedoresGrupos.SingleOrDefault(o => o.Codigo == "C").Lutadores.First(),
                                                                                    listaVencedoresGrupos.SingleOrDefault(o => o.Codigo == "D").Lutadores.Last() })
                                                                                    .First());

                grupoCD.Lutadores.Add(this.OrdenarVencedor(new List<LutadorMOD>() { listaVencedoresGrupos.SingleOrDefault(o => o.Codigo == "C").Lutadores.Last(),
                                                                                    listaVencedoresGrupos.SingleOrDefault(o => o.Codigo == "D").Lutadores.First() })
                                                                                    .First());
                listaVencedoresQuartas.Add(grupoAB);
                listaVencedoresQuartas.Add(grupoCD);

                return listaVencedoresQuartas;
            }
            catch (Exception erro)
            {

                throw;
            }
        }

        public List<GrupoMOD> ObterVencedoresSemifinal(List<GrupoMOD> listaVencedoresQuartas)
        {
            try
            { 
                List<GrupoMOD> listaVencedoresSemifinal = new List<GrupoMOD>();

                GrupoMOD grupoAB = new GrupoMOD();
                grupoAB.Codigo = "AB";
                List<LutadorMOD> listaLutadoresAB = this.OrdenarVencedor(listaVencedoresQuartas.SingleOrDefault(o => o.Codigo == "AB").Lutadores);
                grupoAB.Lutadores = new List<LutadorMOD>();
                grupoAB.Lutadores.Add(listaLutadoresAB.First());

                GrupoMOD grupoCD = new GrupoMOD();
                grupoCD.Codigo = "CD";
                List<LutadorMOD> listaLutadoresCD = this.OrdenarVencedor(listaVencedoresQuartas.SingleOrDefault(o => o.Codigo == "CD").Lutadores);
                grupoCD.Lutadores = new List<LutadorMOD>();
                grupoCD.Lutadores.Add(listaLutadoresCD.First());


                GrupoMOD grupoTerceiroLugar = new GrupoMOD();
                grupoTerceiroLugar.Codigo = "3L";
                grupoTerceiroLugar.Lutadores = new List<LutadorMOD>();
                grupoTerceiroLugar.Lutadores.Add(this.OrdenarVencedor(new List<LutadorMOD>() { listaLutadoresAB.Last(), listaLutadoresCD.Last()}).First());


                listaVencedoresSemifinal.Add(grupoAB);
                listaVencedoresSemifinal.Add(grupoCD);
                listaVencedoresSemifinal.Add(grupoTerceiroLugar);

                return listaVencedoresSemifinal;
            }
            catch (Exception erro)
            {

                throw;
            }
        }

        public List<LutadorMOD> ObterVencedoresTorneio(List<GrupoMOD> listaVencedoresSemi)
        {
            try
            {
                List<LutadorMOD> listaVencedoresFinal = new List<LutadorMOD>();

                listaVencedoresFinal = this.OrdenarVencedor(new List<LutadorMOD>() { listaVencedoresSemi.SingleOrDefault(o => o.Codigo == "AB").Lutadores.First(), listaVencedoresSemi.SingleOrDefault(o => o.Codigo == "CD").Lutadores.First() });

                listaVencedoresFinal.Add(listaVencedoresSemi.SingleOrDefault(o => o.Codigo == "3L").Lutadores.First());

                return listaVencedoresFinal;
            }
            catch (Exception erro)
            {

                throw;
            }
        }

        public List<LutadorMOD> OrdenarVencedor(List<LutadorMOD> listaLutadores)
        {
            try
            {
                //Vitória: maior % de vitórias. Avaliação do número inteiro, não decimal. (vitórias / total de lutas * 100.)
                //Desempate: 1-Qtde Artes Marciais ; 2-Qtde lutas

                return listaLutadores.OrderByDescending(o => Convert.ToInt32((o.Vitorias / o.Lutas) * Convert.ToDouble(100)))
                                     .ThenByDescending(o => o.ArtesMarciais.Count())
                                     .ThenByDescending(o => o.Lutas).ToList();
            }
            catch (Exception erro)
            {

                throw;
            }
        }

        public bool VerificarQuantidadeLutadores(FormCollection form)
        {
            try
            {
                if (!form.AllKeys.Contains("checkboxLutador"))
                {
                    TempData["Alerta"] = String.Concat("Nenhum lutador selecionado.");

                    return false;
                }
                else
                {
                    List<int> lutadoresSelecionados = form["checkboxLutador"].Split(',').Select(o => int.Parse(o)).ToList();

                    if (lutadoresSelecionados.Count != 20)
                    {
                        TempData["Alerta"] = String.Concat("Quantidade incorreta, selecione 20 lutadores.");

                        return false;
                    }
                    else
                        return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}