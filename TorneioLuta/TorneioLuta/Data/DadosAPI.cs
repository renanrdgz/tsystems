﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using TorneioLuta.Models;

namespace TorneioLuta.Data
{
    public class DadosAPI
    {
        public static List<LutadorMOD> ObterLutadoresAPI()
        {
            try
            {
                string lutadoresJSON = String.Empty;

                using (WebClient client = new WebClient())
                {
                    var dadosAPI = client.DownloadData(ConfigurationManager.AppSettings["URLAPI"]);
                    lutadoresJSON = Encoding.UTF8.GetString(dadosAPI);
                }

                List<LutadorMOD> listaLutadores = JsonConvert.DeserializeObject<List<LutadorMOD>>(lutadoresJSON);

                return listaLutadores;
            }
            catch (Exception erro)
            {

                throw;
            }
        }
    }
}